# Visualization of Irrational PI in Complex Plane

The provided Python code generates and visualizes the trajectory of a complex number sequence in the complex plane using matplotlib and numpy. The trajectory is created based on a mathematical expression involving cosine and sine functions.


## Function for Visualizing the Irrationality of Pi

The function $\(z(\theta) = \exp(\theta \cdot i) + \exp(\theta \cdot \pi \cdot i)\)$ has been employed in a Python visualization to highlight certain properties related to the irrationality of $\(\pi\)$.


### Pi and Irrationality

The presence of $\(\pi\)$ in the second exponential term indicates a direct connection to the mathematical constant $\(\pi\)$. If $\(\pi\)$ is irrational, then $\(\theta \cdot \pi\)$ will also be an irrational multiple of $\(\pi\)$. This leads to complex and potentially non-repeating behavior in the function.

### Periodicity and Non-Periodicity

The behavior of the function may vary depending on the rationality or irrationality of $\(\pi\)$. If $\(\pi\)$ is rational, the function might exhibit periodic behavior. Conversely, if $\(\pi\)$ is irrational, the function will showcase non-repeating patterns, emphasizing the unique nature of irrational numbers.


## Code Organization

- The code starts with importing necessary dependencies.
- Parameters like `N` (number of points) and the domain are defined.
- Real and imaginary components of the complex numbers are calculated.
- Graphical plots are created for different visualizations, each in a separate figure.

Note: Some parts of the code are commented out, and the descriptions provide context for both the commented and uncommented sections.
development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

## Graphical Plots

The trajectory is visualized in three different ways, each with a corresponding plotting approach:

1. **Fast Result:**
   - A scatter plot of the complex numbers in a black background.

2. **Slow Result (Colored):**
   - A scatter plot where each point is colored based on a rainbow colormap. The colors represent the progression of points in the sequence.

3. **Slowest Result (Colored and Animated):**
   - An animated scatter plot where the points are added one by one, creating an animated visualization of the trajectory. This is achieved using a loop and the `display` function.
