# -*- coding: utf-8 -*-

# Import dependencies
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm



# Calculation
N = 1000
domain = np.linspace(0, 4*2*np.pi, N)
halfDomain = domain/2

#z = c + s
real = 2 * np.cos( halfDomain*(1+np.pi) ) * np.cos( halfDomain*(1-np.pi) )
imaginary = 2 * np.sin( halfDomain*(1+np.pi) ) * np.cos( halfDomain*(1-np.pi) )



# Graphical plots
colors = cm.rainbow(np.linspace(0, 1, N))
plt.figure(figsize=(10,6))

# Result [Fast]
"""
fig1, ax1 = plt.subplots(figsize=(7, 7), facecolor = 'black')
ax1.set(xlim=[-2, 2], ylim=[-2, 2], aspect=1)
ax1.set_axis_off()
ax1.scatter(real, imaginary, s=0.5, color = 'white')
"""


# Result colored [Slow]

fig2, ax2 = plt.subplots(figsize=(7, 7), facecolor = 'black')
ax2.set(xlim=[-2, 2], ylim=[-2, 2], aspect=1)
ax2.set_axis_off()

i=0
for y, c in zip(imaginary, colors):
    ax2.scatter(real[i], y, s=1, color=c)
    i = i+1


# Result colored animated [Slowest] 
"""
fig3, ax3 = plt.subplots(figsize=(7, 7), facecolor = 'black')
ax3.set(xlim=[-2, 2], ylim=[-2, 2], aspect=1)
ax3.set_axis_off()
for i in range(N+1):
    ax3.scatter(real[:i], imaginary[:i], color=colors[:i], s=1)
    display(fig3)
"""